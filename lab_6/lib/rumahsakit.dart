import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Rumah Sakit',
      theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primaryColor: const Color.fromRGBO(124, 193, 172, 1),
          canvasColor: Colors.white,
          buttonColor: const Color.fromRGBO(253, 175, 103, 1),
          cardColor: const Color.fromRGBO(244, 244, 244, 1),
          cardTheme: CardTheme(
              shadowColor: Colors.grey.shade700,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20)),
              )),
          fontFamily: GoogleFonts.sora().fontFamily,
          textTheme: const TextTheme(
              headline1: TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: 28,
                color: Colors.black,
              ),
              headline2: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  color: Colors.black),
              bodyText1: TextStyle(
                fontSize: 14,
              ),
              bodyText2: TextStyle(
                fontSize: 12,
              ))),
      home: const MyHomePage(title: 'Rumah Sakit'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text("ZONA HIJAU"),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: Color.fromRGBO(124, 193, 172, 1),
              ),
              child: Text(
                'ZONA HIJAU APP',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                ),
              ),
            ),
            ListTile(
              leading: Icon(Icons.design_services),
              title: Text("INFOGRAFIS"),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.quiz),
              title: Text('MITOS / FAKTA'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.medical_services),
              title: Text('LIST RS'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.comment),
              title: Text('FUFU'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(0, 20, 0, 15),
              height: 100,
              width: MediaQuery.of(context).size.width,
              color: Colors.transparent,
              child: Column(
                children: [
                  Text(
                    "Daftar Rumah Sakit",
                    style: Theme.of(context).textTheme.headline1,
                  ),
                  Text(
                    "Berikut Daftar Rumah Sakit Rujukan Covid-19",
                    style: GoogleFonts.sora(),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    height: 35,
                    width: MediaQuery.of(context).size.width / 3,
                    child: TextField(
                      decoration: InputDecoration(
                        suffixIcon: Icon(Icons.search),
                        contentPadding: EdgeInsets.all(1),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                        hintText: " Lokasi...",
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              height: 220,
              width: double.maxFinite,
              child: Card(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                      child: Text(
                        "Nama Rumah Sakit",
                        style: Theme.of(context).textTheme.headline2,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    const ListTile(
                      leading: Icon(
                        Icons.call,
                        size: 27,
                      ),
                      title: Text("XXX-XXX-XXX"),
                    ),
                    const ListTile(
                      leading: Icon(
                        Icons.place_outlined,
                        size: 27,
                      ),
                      title: Text(
                          "Jl. Raya Cileungsi-Jonggol Km.10 Cileungsi Kab. Bogor"),
                    )
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              height: 220,
              width: double.maxFinite,
              child: Card(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                      child: Text(
                        "Nama Rumah Sakit",
                        style: Theme.of(context).textTheme.headline2,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    const ListTile(
                      leading: Icon(
                        Icons.call,
                        size: 27,
                      ),
                      title: Text("XXX-XXX-XXX"),
                    ),
                    const ListTile(
                      leading: Icon(
                        Icons.place_outlined,
                        size: 27,
                      ),
                      title: Text(
                          "Jl. Raya Cileungsi-Jonggol Km.10 Cileungsi Kab. Bogor"),
                    )
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              height: 220,
              width: double.maxFinite,
              child: Card(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                      child: Text(
                        "Nama Rumah Sakit",
                        style: Theme.of(context).textTheme.headline2,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    const ListTile(
                      leading: Icon(
                        Icons.call,
                        size: 27,
                      ),
                      title: Text("XXX-XXX-XXX"),
                    ),
                    const ListTile(
                      leading: Icon(
                        Icons.place_outlined,
                        size: 27,
                      ),
                      title: Text(
                          "Jl. Raya Cileungsi-Jonggol Km.10 Cileungsi Kab. Bogor"),
                    )
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              height: 220,
              width: double.maxFinite,
              child: Card(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                      child: Text(
                        "Nama Rumah Sakit",
                        style: Theme.of(context).textTheme.headline2,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    const ListTile(
                      leading: Icon(
                        Icons.call,
                        size: 27,
                      ),
                      title: Text("XXX-XXX-XXX"),
                    ),
                    const ListTile(
                      leading: Icon(
                        Icons.place_outlined,
                        size: 27,
                      ),
                      title: Text(
                          "Jl. Raya Cileungsi-Jonggol Km.10 Cileungsi Kab. Bogor"),
                    )
                  ],
                ),
              ),
            ),
            FloatingActionButton.extended(
              onPressed: () {},
              label: const Text("Tambah RS"),
              icon: const Icon(Icons.add),
              backgroundColor: Theme.of(context).buttonColor,
            ),
            SizedBox(
              height: 70,
            )
          ],
        ),
      ),
    );
    // This trailing comma makes auto-formatting nicer for build methods.
  }
}
