from django import forms
from django.forms import fields, widgets
from django.forms.formsets import formset_factory
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = '__all__'


    to_attrs = {
        'type':'text',
        'placeholder':'Nama Penerima',
        'class':'form-control'
    }
    from_attrs = {
        'type':'text',
        'placeholder':'Nama Pengirim',
        'class':'form-control'
    }
    title_attrs = {
        'type':'text',
        'placeholder':'Judul Pesan',
        'class':'form-control'
    }
    note_attrs ={
        'type':'textarea',
        'Placeholder':'Tulis Pesan Anda . . .',
        'class':'form-control'

    }

    to = forms.CharField(label="To",required=True,
    max_length=50,widget=forms.TextInput(attrs=to_attrs))

    sender = forms.CharField(label="From",required=True,
    max_length=50,widget=forms.TextInput(attrs=from_attrs))

    title = forms.CharField(label="Title",required=True,
    max_length=50,widget=forms.TextInput(attrs=title_attrs))

    message = forms.CharField(widget=forms.Textarea(attrs=note_attrs))
