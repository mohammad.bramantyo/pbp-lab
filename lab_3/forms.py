from django import forms
from django.forms import fields
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = '__all__'
    error_messages = {
        'required':'Please type'
    }
    nama_attrs = {
        'type':'text',
        'placeholder':'Nama Teman'
    }
    npm_attrs = {
        'type':'text',
        'placeholder':'NPM Teman'
    }
    dob_attrs={
        'type':'date'
    }

    name = forms.CharField(label="Nama",required=True,
        max_length=50,widget=forms.TextInput(attrs=nama_attrs))
    npm = forms.CharField(label="NPM",required=True,
        max_length=10,widget=forms.TextInput(attrs=npm_attrs))
    DOB = forms.DateField(label="Tanggal Lahir",
        widget=forms.DateInput(attrs=dob_attrs))
    