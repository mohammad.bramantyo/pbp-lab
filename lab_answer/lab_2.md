## Jawaban 

1. Apakah perbedaan JSON dan XML ?  
    JSON merupakan *Javasscript object notation* sedangkan XML merupakan *eXstensible Markup Language*. Json sendiri merupakan sebuah cara untuk menampilkan data sedangkan xml merupakan markup language yang menggunakan truktur tag untuk menampilkan data. 

   Secara penyimpanan json mensupport penggunaan array sementara xml tidak. Json lebih mudah dibaca dibanding xml. 

2. Apakah perbedaan antara HTML dan XML ?  
    Secara nama mereka merupakan markup language namun ada beberapa hal yang berbeda. HTML merupakan static files sedangkan XML dynamic karena memabawa data dan dapat berubah. Kemudian untuk tags, HTML memiliki keterbatasan tags sedangkan XML tidak. 

    Tags yang digunakan pada HTML untuk menampilkan data sedangkan pada XML untuk menjelaskan suatu data. HTML tidak *case sensisitive* sementara XML *case sensitive*
