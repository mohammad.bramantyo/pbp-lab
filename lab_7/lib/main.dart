import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Sign Up",
    home: BelajarForm(),
    theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: const Color.fromRGBO(124, 193, 172, 1),
        fontFamily: 'Sora',
        buttonColor: const Color.fromRGBO(253, 175, 103, 1),
        cardColor: const Color.fromRGBO(244, 244, 244, 1),
        textTheme: const TextTheme(
          headline1: TextStyle(
              fontSize: 25, fontWeight: FontWeight.bold, color: Colors.black),
        )),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  List<String> listDaerah = [
    'Jakarta',
    'Bogor',
    'Depok',
    'Tangerang',
    'Bekasi'
  ];
  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;
  String value = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Form Rumah Sakit"),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: Center(
          child: Container(
        width: 450,
        child: Card(
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "Tambah  Rumah Sakit",
                        style: Theme.of(context).textTheme.headline1,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        decoration: new InputDecoration(
                          hintText: "contoh: Susilo Bambang",
                          labelText: "Nama Rumah Sakit",
                          fillColor: Colors.white,
                          filled: true,
                          border: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0)),
                          focusColor: Color(0xFFDEB887),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Mohon isi field ini';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: DropdownButtonFormField(
                            decoration: new InputDecoration(
                              fillColor: Colors.white,
                              filled: true,
                              border: OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(5.0)),
                            ),
                            hint: Text("Daerah"),
                            items: listDaerah.map((daerah) {
                              return DropdownMenuItem(
                                value: daerah.toString(),
                                child: Text('Kota $daerah'),
                              );
                            }).toList())),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        decoration: new InputDecoration(
                          labelText: "Alamat",
                          fillColor: Colors.white,
                          filled: true,
                          border: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'tidak boleh kosong';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        decoration: new InputDecoration(
                          labelText: "Nomor Telepon",
                          fillColor: Colors.white,
                          filled: true,
                          border: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'tidak boleh kosong';
                          }
                          return null;
                        },
                      ),
                    ),
                    SizedBox(
                      width: 160,
                      height: 40,
                      child: RaisedButton(
                        child: Text(
                          "Tambahkan",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: Theme.of(context).buttonColor,
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(23.0),
                        ),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            print("Nambah gan");
                          }
                          _formKey.currentState?.reset();
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      )),
    );
  }
}
